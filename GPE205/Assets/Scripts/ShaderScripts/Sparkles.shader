﻿Shader "Unlit/Sparkles"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}

		_SparkleTex("Sparkle Texture", 2D) = "white"{}

		_Scale("Scale", Float) = 1
		_SparkleColor("SparkleColor", Color) = (1,1,1,1)
		_Intensity("Intensity", Float) = 50

		_RimColor("RimColor", Color) = (1,1,1,1)
		_RimPower("RimPower", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 wPos : TEXCOORD1;
				float3 wNormal : TEXCOORD2;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _SparkleTex;
			float _Scale;
			half4 _SparkleColor;
			float _Intensity;

			half4 _RimColor;
			float _RimPower;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.wPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				o.wNormal = UnityObjectToWorldNormal(v.normal);

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				
				half3 viewDirection = normalize(i.wPos - _WorldSpaceCameraPos);

				fixed3 sparklemap = tex2D(_SparkleTex, i.uv*_Scale);
				sparklemap -= half3(0.5, 0.5, 0.5);

				sparklemap = normalize(sparklemap);
				half sparkle = pow(saturate((dot(-viewDirection, normalize(sparklemap + i.wNormal)))), _Intensity);

				col += _SparkleColor * sparkle;

				half rim = pow(1.0 - saturate(dot(-viewDirection, i.wNormal)), _RimPower);

				col += _RimColor * rim;

				return col;
			}
			ENDCG
		}
	}
}
