﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    //holds the offset
    public Vector3 offset;
    //holds the turnspeed
    public float turnSpeed;
    //holds the players transform
    [SerializeField]
    private Transform player;

	// Update is called once per frame
	void LateUpdate ()
	{
        //sets the cameras position
        //grabs the x axis of the mouse 
	    offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
        //updates the position with the offset
	    transform.position = player.transform.position + offset;
        //has the camera look at that position
        transform.LookAt(player.position);
	}
}
