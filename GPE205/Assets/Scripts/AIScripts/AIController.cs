﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(TankCannon))]
[RequireComponent(typeof(TankGuns))]
public class AIController : MonoBehaviour {

    //variables for other class scripts
    private TankData tankData;
    private TankCannon tankCannon;
    private TankGuns tankGuns;
    private NavMeshAgent agent;
    private List<Transform> targets = new List<Transform>();
    private Transform target;
    private float cannonRotateSpeed;

    //public properties
    public float hearingRadius;
    public float visionRange;

    //property to return target
    public Transform Target { get { return target; } }
    //property to return tankdata
    public TankData TankData { get { return tankData; } }
    //property for target
    public bool HasTarget
    {
        get {
            return target != null && target.gameObject.activeInHierarchy;
        }
    }

    //create state machine for AI
    public StateMachine<AIController> stateMachine = new StateMachine<AIController>();
    //Enum of states for AI
    public enum StateType
    {Search, Target, GunAttack, CannonAttack, Chase, FindPickUp}


    void Awake()
    {
        //set the variables to the scripts on this gameobject
        tankCannon = GetComponent<TankCannon>();
        tankGuns = GetComponent<TankGuns>();
        agent = GetComponentInParent<NavMeshAgent>();
        if (tankData == null)
        {
            tankData = GetComponent<TankData>();
        }
        //sets the tank setting from the scriptable object
        TankSettings();

        //add states to state machine dictionary
        stateMachine.states.Add((int)StateType.Search, new SearchState());
        stateMachine.states.Add((int)StateType.Target, new TargetState());
        stateMachine.states.Add((int)StateType.GunAttack, new GunAttackState());
        stateMachine.states.Add((int)StateType.CannonAttack, new CannonAttackState());
        stateMachine.states.Add((int)StateType.Chase, new ChaseState());
        stateMachine.states.Add((int)StateType.FindPickUp, new RetreatState());

        //sets state machine obj to this gameobject
        stateMachine.Init(this);
    }

    private void Start()
    {
        SetTargets();
    }

    //look into couroutine to have the ability to turn ai on  and off
    private void Update()
    {
        if (tankData.CanMove)
        {
            stateMachine.Run();
        }
    }

    //function to check if target is nearby
    public Vector3 ListenForTargets()
    {
        Vector3 possibleTargetPos = Vector3.zero;
        //iterates through targets list
        for (int i = 0; i < targets.Count; i++)
        {
            //finds an active target
            if (targets[i].gameObject.activeInHierarchy)
            {
                //check to see if it is within range
                if (Vector3.Distance(targets[i].position, transform.position) <= hearingRadius)//TODO: Add seeing distance varaible
                {
                    //target found within hearing range
                    target = targets[i];
                    possibleTargetPos = targets[i].position;
                }
            }
        }
        return possibleTargetPos;
    }

    //if target is found rotate to face targets last known position
    public void RotateToFaceTarget()
    {
        Vector3 targetsPos = ListenForTargets();
        targetsPos.y = 0;

        //have the cannon forward lerp to the cameras forward based on the speed
        tankData.cannon.transform.forward = Vector3.Lerp(tankData.cannon.transform.forward, targetsPos, cannonRotateSpeed * Time.deltaTime);
    }

    public void ReturnToSearchPos()
    {
        tankData.cannon.transform.forward = Vector3.Lerp(tankData.cannon.transform.forward, transform.forward, cannonRotateSpeed * Time.deltaTime);
    }

    public void CheckIfTargetInVisionRange()
    {

    }

    //finds all targets for tanks 
    public void SetTargets()
    {
        //grabs all the enemy team tanks
        FindTeam[] teams = FindObjectsOfType<FindTeam>();

        //add them to the targets list
        for (int i = 0; i < teams.Length; i++)
        {
            if (teams[i].team != tankData.team)
            {
                for (int j = 0; j < teams[i].teamMembers.Count; j++)
                {
                    targets.Add(teams[i].teamMembers[j].transform);
                }
            }
        }
        //target = targets[Random.Range(0, targets.Count - 1)];
    }

    //function to find next target
    public void FindNextTarget()
    {
        //iterates through targets list
        for (int i = 0; i < targets.Count; i++)
        {
            //finds an active target
            if (targets[i].gameObject.activeInHierarchy)
            {
                //check to see if it is within range
                if (Vector3.Distance(targets[i].position, transform.position) < 15.0f)//TODO: Add seeing distance varaible
                {
                    //set the target
                    target = targets[i];
                    break;
                }
                else {
                    target = targets[Random.Range(0, targets.Count - 1)];}//set target to random
            }
        }
    }

    //sets the controls for the AI
    public void MoveAI()
    {    
        if (target == null)
            return;
        //move ai to target
        agent.SetDestination(target.position);     
    }

    public void Attack()
    {
        float step = tankData.tankType[(int)tankData.cannonVersion].cannonRotateSpeed * Time.deltaTime;
        Vector3 cannonPosition = tankData.cannon.transform.forward;
        //if its within the area to hear it 
        if (Vector3.Distance(transform.position, target.position) < 10.0f)//TODO: Add hearing distance varaible
        {
            //set rotation of tank
            Vector3 newDir = Vector3.RotateTowards(cannonPosition, target.position - transform.position, step, 0);
            tankData.cannon.transform.rotation = Quaternion.LookRotation(newDir);

            RaycastHit hit;
            //raycast to check if there is an enemy in cannon range
            if (Physics.Raycast(tankData.cannon.transform.position, tankData.cannon.transform.forward, out hit, 20))//TODO: Add range distance varaible
            {
                //variable to store hit
                TankData other = hit.collider.GetComponent<TankData>();
                if (other != null)
                {
                    //checks if its another team
                    if (other.team != tankData.team)
                    {
                        //attack target
                        tankCannon.FireCannon();
                        
                        tankGuns.FireGuns();
                    }
                }
            }
        }
    }

    //Find a random point on navmesh to flee to
    public Vector3 RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }

    //flee to navmesh point
    public Vector3 Flee()
    {
        Vector3 fleePoint = RandomNavmeshLocation(20f);
        agent.SetDestination(fleePoint);//TODO: Add flee distance varaible
        return fleePoint;
    }

    //function to check if health is low based on max health divided by 2
    public void LowHealth()
    {
        if(tankData.HealthData <= (tankData.HealthData / 2))
        {
            stateMachine.CurrentState = (int)StateType.FindPickUp;
        }
    }

    //sets the type of cannon is being used by the tanks
    public void TankSettings()
    {
        //takes in tank data from scriptable object and sets all the tank settings
        tankCannon.shellDamage = tankData.tankType[(int)tankData.cannonVersion].cannonShellDamage;
        tankGuns.bulletDamage = tankData.tankType[(int)tankData.cannonVersion].bulletDamage;
        tankData.HealthData = tankData.tankType[(int)tankData.cannonVersion].tankHealth;
        tankData.ArmourData = tankData.tankType[(int)tankData.cannonVersion].tankArmour;
        agent.speed = tankData.tankType[(int)tankData.cannonVersion].tankForwardSpeed;
        agent.angularSpeed = tankData.tankType[(int)tankData.cannonVersion].tankRotateSpeed;
        cannonRotateSpeed = tankData.tankType[(int)tankData.cannonVersion].cannonRotateSpeed;
        //finds the children in the cannon for the tank
        tankData.cannonPieces = tankData.cannon.GetComponentsInChildren<Renderer>();
        //sets each child in the cannon to have the cannon material for the tank type
        foreach (Renderer renderer in tankData.cannonPieces)
        { renderer.material = tankData.tankType[(int)tankData.cannonVersion].tankMat; }

        //changes team materials
        tankData.bodyPieces = tankData.body.GetComponentsInChildren<Renderer>();
        foreach (Renderer renderer in tankData.bodyPieces)
        { renderer.material = tankData.teamMats[(int)tankData.teamMat]; }
    }
}
