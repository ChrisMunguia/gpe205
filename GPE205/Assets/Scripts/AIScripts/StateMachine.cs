﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State<T> where T : MonoBehaviour
{
    abstract public void OnRun(T obj);
}

public class StateMachine<T> where T : MonoBehaviour
{
    public Dictionary<int , State<T>> states = new Dictionary<int, State<T>>();

    private T obj;

    public void Init(T obj)
    {
        this.obj = obj;
    }

    private int currentState = 0;
    public int CurrentState
    {
        get { return currentState; }
        set
        {
            if(currentState != value)
            {
                currentState = value;
            }
        }
    }
    public void Run()
    {
        states[currentState].OnRun(obj);
    }
}