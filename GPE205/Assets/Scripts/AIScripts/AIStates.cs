﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Namespace look up
//State for searching for target
//have constant check if target is dead to change switch to search state
//have constant check if health is low to retreat
//look into running multiple states at a time
public class SearchState : State<AIController>
{
    public override void OnRun(AIController obj)
    {
        //run patrol funtion to try and find a target
        obj.ListenForTargets();
        if (obj.HasTarget)
        {
            obj.stateMachine.CurrentState = (int)AIController.StateType.Target;
        }
    }
}

public class TargetState : State<AIController>
{
    public override void OnRun(AIController obj)
    {
        //rotate to face target
        obj.RotateToFaceTarget();
        if(Vector3.Distance(obj.Target.position, obj.transform.position) <= obj.visionRange)
        {

        }
        else
        {
            obj.ReturnToSearchPos();
        }
        //if withing gun range switch to gun attack state
        //if within cannon range switch to cannon attack range
        //if target is out of range
        //switch to chase state
    }
}

//State for attacking enemies
public class GunAttackState : State<AIController>
{
    public override void OnRun(AIController obj)
    {
        //fire gun at target
        //if target is no longer in range
        //swtich to chase state
    }
}

//State for attacking enemies
public class CannonAttackState : State<AIController>
{
    public override void OnRun(AIController obj)
    {
        //fire cannon at target
        //if target is no longer in range
        //swtich to chase state
    }
}

//State for chasing target
public class ChaseState : State<AIController>
{
    public override void OnRun(AIController obj)
    {
        //move to target position until target is within stopping distance
        //if target is within stopping distance
        //swtich to target state
    }
}

//State for Retreating
public class RetreatState : State<AIController>
{

    public override void OnRun(AIController obj)
    {
        //locate pickup object on map
        //move toward pickup object
        //if health is at decent level
        //swtich to search state
    }
}

