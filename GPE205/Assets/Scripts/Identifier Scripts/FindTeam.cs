﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindTeam : MonoBehaviour {

    [Header("Team Members")]
    public TankData.Team team = TankData.Team.BlueTeam;

    public List<Transform> teamMembers;

    private void Awake()
    {
        SetTeams();
    }

    //finds all the targets on the enemy team
    void SetTeams()
    {
        //finds all the gamobjects with tank data
        TankData[] possibleTeamMembers = FindObjectsOfType<TankData>();

        switch (team)
        {
            //finds targets for red team
            case TankData.Team.BlueTeam:

                //find all possible members and adds them to the list
                for (int i = 0; i < possibleTeamMembers.Length; i++)
                {
                    if (possibleTeamMembers[i].team == TankData.Team.BlueTeam)
                        teamMembers.Add(possibleTeamMembers[i].transform);
                }
                break;

            //finds targets for blue team
            case TankData.Team.RedTeam:

                //finds all possible members and adds them to the list
                for (int i = 0; i < possibleTeamMembers.Length; i++)
                {
                    if (possibleTeamMembers[i].team == TankData.Team.RedTeam)
                        teamMembers.Add(possibleTeamMembers[i].transform);
                }
                break;
            case TankData.Team.GreenTeam:
                //finds all possible members and adds them to the list
                for (int i = 0; i < possibleTeamMembers.Length; i++)
                {
                    if (possibleTeamMembers[i].team == TankData.Team.GreenTeam)
                        teamMembers.Add(possibleTeamMembers[i].transform);
                }
                break;
            case TankData.Team.PurpleTeam:
                //finds all possible members and adds them to the list
                for (int i = 0; i < possibleTeamMembers.Length; i++)
                {
                    if (possibleTeamMembers[i].team == TankData.Team.PurpleTeam)
                        teamMembers.Add(possibleTeamMembers[i].transform);
                }
                break;
        }
    }
}
