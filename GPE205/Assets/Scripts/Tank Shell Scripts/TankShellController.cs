﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankShellData))]
public class TankShellController : MonoBehaviour
{
    private Rigidbody rb;
    private Transform tf;
    private TankShellData tankShellData;

	// Use this for initialization
	void Start ()
	{
        rb = GetComponent<Rigidbody>();
        tf = GetComponent<Transform>();
	    tankShellData = GetComponent<TankShellData>();
        //start timer
	    StartCoroutine(DestroyShell());
        //add force to the shell when it spawns
        rb.AddForce(tf.forward * tankShellData.force, ForceMode.Force);
    }

    //checks to see if shell collides
    public void OnCollisionEnter(Collision collision)
    {
        //stores the tank that the shell hit
        TankData collidedTank = collision.gameObject.GetComponent<TankData>();
        //check if it's a player 
        if (collidedTank != null)
        {
            //do damage to the tanks health property based on the damage value
            //from the cannon
            collidedTank.HealthData -= tankShellData.TankCannonDamage;
            //if the tank that was hit's property is at 0 or less than
            if(collidedTank.HealthData <= 0)
            {
                //add points to parent tank
                tankShellData.parentTank.tankScore += collidedTank.tankType[(int)collidedTank.cannonVersion].pointsWorth; 
            }
        }
        //set shell inactive
        Destroy(gameObject);
    }

    //set shell inactive after a while
    IEnumerator DestroyShell()
    {
        //time to wait
        yield return new WaitForSeconds(tankShellData.timeToDespawn);
        //set shell inactive
        Destroy(gameObject);
    }
}
