﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShellData : MonoBehaviour
{
    //TODO: Fix this shit
    [SerializeField]
    private float tankCannonDamage;
    //variable for shell damage set in tank cannon
    public float TankCannonDamage {
        get { return tankCannonDamage; }
        set { tankCannonDamage = value; }
    }

    //variable for shell to despawn
    [Header("Time Till Shell To DeSpawn")]
    public float timeToDespawn;

    //variable for force applied to shell
    [Header("Speed Of Bullet")]
    public float force = 10.0f;

    [HideInInspector]
    //grabs the parent tank that fires the bullet
    //TODO:look into making this a property
    public TankData parentTank;

}
