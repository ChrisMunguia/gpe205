﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TankData : MonoBehaviour
{
    
    [Header("Tanks Camera")]
    //camera attached to player
    public Camera cam;

    [Header("Tank Retical")]
    //tanks aim point
    public GameObject gunRetical;
    public GameObject cannonTargetRetical;
    public Image reloadRetical;

    [Header("UI Data")]
    //health component of Tank
    public Canvas uiDataCanvas;
    public Image healthBar;
    public TextMeshProUGUI healthText;
    public Image armourBar;
    public TextMeshProUGUI armourText;

    [Header("Main Cannon")]
    //cannon to rotate
    public GameObject cannon;
    public Component[] cannonPieces;

    [Header("Main Body")]
    public GameObject body;
    public Component[] bodyPieces;

    [Header("Cannon Fire Points")]
    //array of tank cannon fire points
    public Transform[] cannonPoints;

    [Header("Gun Fire Points")]
    //array of tank gun fire points
    public Transform[] gunpoints;

    [Header("Tank Score")]
    public float tankScore;

    //TODO: have teams be dictionary identifiers for team colors to be 
    //set automatically
    public enum Team
    { BlueTeam, RedTeam, GreenTeam, PurpleTeam}
    [Header("Current Team")]
    public Team team = Team.BlueTeam;

    public enum Colors
    { blueTeamMat, redTeamMat, greenTeamMat, purpleTeamMat }
    [Header("Team Color")]
    public Colors teamMat = Colors.blueTeamMat;
    //team materials
    public Material[] teamMats;

    //put in enum for tank shell type based on cannon level
    public enum CannonVersion
    { light, medium, heavy }
    //sets the basic cannon per tank to light
    [Header("Tank Type")]
    public CannonVersion cannonVersion = CannonVersion.light;
    //array of scriptable object tank types
    public TankTypeData[] tankType;

    //enum for types of inputs
    public enum Inputs
    { WASD, arrowKeys, controller };
    //sets the current input scheme in the editor
    [Header("Input Scheme")]
    public Inputs input = Inputs.WASD;
    //array for scriptable object input schemes
    public InputScheme[] inputTypes;

    //to test tanks
    //TODO: Remove once unneaded
    public bool CanMove = true;

    private float healthData;
    private float armourData;

    //property for tank health
    public float HealthData
    {
        //returns the current health
        get { return healthData; }
        set
        {
            //checks if armour is at 0
            float dif = healthData - value;
            if ((dif > 0) && (armourData > 0))
            { ArmourData -= dif; }//does damage to armour if tank has it
            else
            { healthData = value; }//if no armour do damage to health

            float newHealthData = Mathf.Clamp(HealthData, 0, tankType[(int)cannonVersion].tankHealth);
            healthBar.fillAmount = newHealthData / tankType[(int)cannonVersion].tankHealth;
            healthText.text = newHealthData + " / " + tankType[(int)cannonVersion].tankHealth;

            //check if tank health is 0
            if (healthData <= 0)
            {
                gameObject.SetActive(false);//kill tank
                uiDataCanvas.gameObject.SetActive(false);
            }
        }
    }

    //propertiey for tank armour
    public float ArmourData
    {
        //returns current armour 
        get { return armourData; }
        set { armourData = value;
            float newArmourData = Mathf.Clamp(ArmourData, 0, tankType[(int)cannonVersion].tankArmour);
            armourBar.fillAmount = newArmourData / tankType[(int)cannonVersion].tankArmour;
            armourText.text = newArmourData + " / " + tankType[(int)cannonVersion].tankArmour;
        }//sets the armour 
    }
}
