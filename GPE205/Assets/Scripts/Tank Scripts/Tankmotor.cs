﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Tankmotor : MonoBehaviour
{
    //component variables
    private CharacterController cc;
    private Transform tf;
    private TankData tankData;

    void Awake()
    {
        //stores components on awake
        cc = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
        tankData = GetComponent<TankData>();
        tankData.cannon = gameObject.GetComponentInChildren<Cannon>().gameObject;
    }

    //move function
    public void Move(float speed)
    {
        //local variable
        Vector3 speedVector;

        //sets the speed vector to speed times forward
        speedVector = tf.forward * speed;

        //moves object based on speed vector
        cc.SimpleMove(speedVector);
    }

    //rotate function
    public void Rotate(float speed)
    {
        //local variable
        Vector3 rotateVector;

        //sets rotate vector
        rotateVector = Vector3.up * speed * Time.deltaTime;

        //rotates object based on vector in its local space
        tf.Rotate(rotateVector, Space.Self);
    }

}
