﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCannon : MonoBehaviour
{
    //shell damage property
    public float shellDamage { get; set; }

    //bool for if cannon can fire
    private bool canFire = true;
    private TankData tankData;
    private GameObject shell;
    private float cannonFireDelay;

    void Awake()
    {
        tankData = GetComponent<TankData>();
        //sets shell variable to the tankdatas variable
        shell = tankData.tankType[(int)tankData.cannonVersion].tankShell;
        //grab the cannonfire delay from tank data
        cannonFireDelay = tankData.tankType[(int)tankData.cannonVersion].cannonFireDelay;
    }

    //function to have the cannon fire on delay
    public void FireCannon()
    {
        //checks to see if cannon timer has been reset
        if (canFire)
        {
            tankData.reloadRetical.fillAmount = 0;
            //starts coroutine
            StartCoroutine(FireRate(cannonFireDelay));
        }
    }

    //this instantiates the shell and starts timer
    IEnumerator FireRate(float delay)
    {
        float totalTime = 0;
        // note set up pool for shells to spawn from
        canFire = false;
        //iterate through cannon array
        foreach (Transform cannonPoint in tankData.cannonPoints)
        //instantiate shell to fire
        { Instantiate(shell, cannonPoint.position, cannonPoint.rotation); }
        //set the damage of the shell based on the tank that fired it
        shell.GetComponent<TankShellData>().TankCannonDamage = shellDamage;
        //let the shell that was fired know who fired it
        shell.GetComponent<TankShellData>().parentTank = tankData;
        while (totalTime <= delay)
        {
            tankData.reloadRetical.fillAmount = totalTime / delay;
            totalTime += Time.deltaTime;
            yield return null;
        }
        canFire = true;
    }
}
