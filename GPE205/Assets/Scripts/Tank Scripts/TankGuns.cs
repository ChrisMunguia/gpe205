﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankGuns : MonoBehaviour {

    //bullet damage property
    public float bulletDamage { get; set; }

    private bool canFire = true;
    private TankData tankData;
    private GameObject bullet;
    private float gunFireDelay;

	// Use this for initialization
	void Awake () {

        tankData = GetComponent<TankData>();
        //grabs the bullet prefab to fire from the tank data
        bullet = tankData.tankType[(int)tankData.cannonVersion].tankBullet;
        //grabs the bullets fire delay from the tank data
        gunFireDelay = tankData.tankType[(int)tankData.cannonVersion].gunFireDelay;
	}
	
    //function to fire guns
	public void FireGuns()
    {
        if (canFire)
        {
            //runs gun fire coroutine
            StartCoroutine(GunFireRate(gunFireDelay));
        }
    }

    //coroutine to fire bullets based on fire rate
    IEnumerator GunFireRate(float delay)
    {
        canFire = false;
        //iterate through all guns in array
        foreach (Transform gunPoint in tankData.gunpoints)
        //instantiate bullets
        { Instantiate(bullet, gunPoint.position, gunPoint.rotation); }
        //sets the damage of the bullets
        bullet.GetComponent<TankShellData>().TankCannonDamage = bulletDamage;
        //lets the bullet know who is firing it
        bullet.GetComponent<TankShellData>().parentTank = tankData;
        yield return new WaitForSeconds(delay);
        canFire = true;
    }
}
