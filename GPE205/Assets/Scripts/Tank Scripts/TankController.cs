﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Tankmotor))]
[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(TankCannon))]
[RequireComponent(typeof(TankGuns))]
public class TankController : MonoBehaviour
{
    //variables for other class scripts
    private Tankmotor tankMotor;
    private TankData tankData;
    private TankCannon tankCannon;
    private TankGuns tankGuns;
    private List<Transform> targets = new List<Transform>();

    void Awake()
    {
        //set the variables to the scripts on this gameobject
        tankMotor = GetComponent<Tankmotor>();
        tankCannon = GetComponent<TankCannon>();
        tankGuns = GetComponent<TankGuns>();
        tankData = GetComponent<TankData>();
        //sets the tank setting from the scriptable object
        TankSettings();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Start()
    {
        //find targets for tank
        SetPossibleTargets();
    }

    // Update is called once per frame
    void Update () {
        Controls();//call controls function
	}

    //function to tank inputs
    public void Controls()
    {
        //takes setting from input scheme object and sets the variables
        if (Input.GetKey(tankData.inputTypes[(int)tankData.input].forward))
        { tankMotor.Move(tankData.tankType[(int)tankData.cannonVersion].tankForwardSpeed); }
        if (Input.GetKey(tankData.inputTypes[(int)tankData.input].backward))
        { tankMotor.Move(-tankData.tankType[(int)tankData.cannonVersion].tankReverseSpeed); }
        if (Input.GetKey(tankData.inputTypes[(int)tankData.input].left))
        { tankMotor.Rotate(-tankData.tankType[(int)tankData.cannonVersion].tankRotateSpeed); }
        if (Input.GetKey(tankData.inputTypes[(int)tankData.input].right))
        { tankMotor.Rotate(tankData.tankType[(int)tankData.cannonVersion].tankRotateSpeed); }
        if (Input.GetKey(tankData.inputTypes[(int)tankData.input].cannonFire))
        { tankCannon.FireCannon(); }
        if (Input.GetKey(tankData.inputTypes[(int)tankData.input].gunFire))
        { tankGuns.FireGuns(); }
        RotateCannon(tankData.tankType[(int)tankData.cannonVersion].cannonRotateSpeed);
    }

    //sets the type of cannon is being used by the tanks
    public void TankSettings()
    {
        //takes in tank data from scriptable object and sets all the tank settings
        tankCannon.shellDamage = tankData.tankType[(int)tankData.cannonVersion].cannonShellDamage;
        tankGuns.bulletDamage = tankData.tankType[(int)tankData.cannonVersion].bulletDamage;
        tankData.HealthData = tankData.tankType[(int)tankData.cannonVersion].tankHealth;
        tankData.ArmourData = tankData.tankType[(int)tankData.cannonVersion].tankArmour;
        //finds the children in the cannon for the tank
        tankData.cannonPieces = tankData.cannon.GetComponentsInChildren<Renderer>();
        //sets each child in the cannon to have the cannon material for the tank type
        foreach (Renderer renderer in tankData.cannonPieces)
        { renderer.material = tankData.tankType[(int)tankData.cannonVersion].tankMat; }

        //change material
        tankData.bodyPieces = tankData.body.GetComponentsInChildren<Renderer>();
        foreach (Renderer renderer in tankData.bodyPieces)
        { renderer.material = tankData.teamMats[(int)tankData.teamMat]; }
    }

    //finds all targets for tanks 
    public void SetPossibleTargets()
    {
        //grabs all the enemy team tanks
        FindTeam[] teams = FindObjectsOfType<FindTeam>();

        //add them to the targets list
        for (int i = 0; i < teams.Length; i++)
        {
            if (teams[i].team != tankData.team)
            {
                for (int j = 0; j < teams[i].teamMembers.Count; j++)
                {
                    targets.Add(teams[i].teamMembers[j].transform);
                }
            }
        }
    }

    //rotates cannon barrel to point in direction of mouse position
    public void RotateCannon(float speed)
    {
        //variable for the camera tfs forward
        Vector3 newCannonPosition = tankData.cam.transform.forward;
        //set y to 0 to only move the x
        newCannonPosition.y = 0;

        //have the cannon forward lerp to the cameras forward based on the speed
        tankData.cannon.transform.forward = Vector3.Lerp(tankData.cannon.transform.forward, newCannonPosition, speed * Time.deltaTime);

        //set retical for cannon
        SetCannonReticals(tankData.cannon.transform.position, tankData.cannon.transform.forward, tankData.cannonTargetRetical, true, speed);
        SetCannonReticals(tankData.cannon.transform.position, tankData.cannon.transform.forward, tankData.reloadRetical.gameObject, false, speed);
        SetCannonReticals(tankData.gunpoints[0].transform.position, tankData.gunpoints[0].transform.forward, tankData.gunRetical, true, speed);
    }

    //function to move retical of player tank
    public void SetCannonReticals(Vector3 raycastStart, Vector3 raycastDirection, GameObject retical, bool changeColor, float speed)
    {
        RaycastHit hit;
        //have the point set to just be infront of the cannon
        Vector3 worldpoint = raycastStart + raycastDirection * 15f;
        //raycast to check if there is an enemy in cannon range
        if (IgnoreRaycast<TankShellData>(raycastStart, raycastDirection, out hit, Mathf.Infinity))
        {
            TankData other = hit.collider.gameObject.GetComponent<TankData>();
            //check to see if the hit is a tank
            if (other != null)
            {
                //check to see if the tank is on a different team
                if (other.team != tankData.team)
                {
                    //set the hit point in world space
                    worldpoint = hit.point;
                    if (changeColor)
                    {
                        //change color of retical to show its an enemy
                        retical.GetComponent<Image>().color = Color.red;
                    }
                }
            }
            //check to see if the hit is a shell or bullet
            else
            {      
                if (changeColor)
                {
                    //change color to be show no enemy
                    retical.GetComponent<Image>().color = Color.green;
                }
            }
        }
        //if nothing is hit keep reticals green
        else
        {
            if (changeColor)
            {
                //change color to be show no enemy
                retical.GetComponent<Image>().color = Color.green;
            }
        }

        //set the retical tf to the set worldpoint in world space
        retical.transform.position = RectTransformUtility.WorldToScreenPoint(tankData.cam, worldpoint);
    }

    //function to ignore monobehaviours
    bool IgnoreRaycast<T>(Vector3 start, Vector3 direction, out RaycastHit hit, float distance) where T : MonoBehaviour
    {
        //local variables
        Vector3 s = start;
        bool finished = false;
        hit = new RaycastHit();
        //if raycast hits an ignored monobehaviour
        while (!finished)
        {
            //re raycast
            if (Physics.Raycast(s, direction, out hit, distance))
            {
                //if monobehaviour is hit again
                if (hit.collider.GetComponent<T>())
                {
                    //reset to new position
                    s = hit.collider.transform.position;
                }
                else
                {
                    finished = true;
                }
            }
            else
            {
                finished = true;
                hit = new RaycastHit();
            }
        }
        return hit.collider != null;
    }
}
