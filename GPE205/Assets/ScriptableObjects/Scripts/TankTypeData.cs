﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tank Data")]
public class TankTypeData : ScriptableObject {

    [Header("Ammunition To Fire")]
    //tank shells fired
    public GameObject tankShell;
    public GameObject tankBullet;

    [Header("Tanks Speed Data")]
    //tank speed variables
    public float tankForwardSpeed;
    public float tankReverseSpeed;
    public float tankRotateSpeed;

    [Header("Tank Type Materials")]
    //cannon material per type
    public Material tankMat;

    [Header("Tanks Health Data")]
    //tank health variables
    public float tankHealth;

    [Header("Tanks Armour Data")]
    //tank armour variable
    public float tankArmour;

    [Header("Points On Tank Death")]
    public float pointsWorth;

    [Header("Weapon Fire Rate")]
    //time to delay cannon fire
    public float cannonFireDelay;
    //time to delay bullet fire
    public float gunFireDelay;

    [Header("Weapon Damage Data")]
    //damage for shells fired
    public float cannonShellDamage;
    //damage for bullets fired
    public float bulletDamage;

    [Header("Cannon Rotate Speed")]
    //speed of cannon rotaion
    public float cannonRotateSpeed;

}
