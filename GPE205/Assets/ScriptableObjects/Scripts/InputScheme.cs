﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Input Scheme")]
public class InputScheme : ScriptableObject
{
    [Header("Controls")]
    public KeyCode forward;
    public KeyCode backward;
    public KeyCode left;
    public KeyCode right;
    public KeyCode cannonFire;
    public KeyCode gunFire;
}
